﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson10_1
{
    class GenericArray<T>
    {
        private T[] array;

        public GenericArray(int size)
        {
            array = new T[size];
        }

        public void AddItem(int index, T item)
        {
            if (index >= 0 && index < array.Length)
            {
                array[index] = item;
            }
        }

        public void RemoveItem(int index)
        {
            if (index >= 0 && index < array.Length)
            {
                array[index] = default(T);
            }
        }

        public T GetItem(int index)
        {
            if (index >= 0 && index < array.Length)
            {
                return array[index];
            }
            return default(T);
        }

        public int GetLength()
        {
            return array.Length;
        }
    }
}

