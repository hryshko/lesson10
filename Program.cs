﻿namespace Lesson10_1
{
     class Program
    {
        static void Main(string[] args)
        {
            GenericArray<int> intArray = new GenericArray<int>(4);

            intArray.AddItem(0, 1);
            intArray.AddItem(1, 2);
          
            Console.WriteLine("The element with index 0: " + intArray.GetItem(0));
            Console.WriteLine("The element with index 1: " + intArray.GetItem(1));
            Console.WriteLine("The element with index 2: " + intArray.GetItem(2));

            intArray.RemoveItem(1);

            Console.WriteLine("The array from length: " + intArray.GetLength());

            Console.WriteLine("An element with index 1 after its deletion: " + intArray.GetItem(1));

            Console.ReadLine();
        } 
    }
}